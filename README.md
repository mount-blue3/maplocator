# Location Search and Display Population Using React and Leaflet



This is a sample React application that allows you to search for a location using the OpenStreetMap Nominatim API and display it on a Leaflet map. The application provides an input field where you can enter a location search term, and it will retrieve a list of matching locations. You can then choose a location from the list, and the map will update to show the selected location's marker and related information.


## Prerequisites

To run this application, you need to have the following dependencies installed:

*   Node.js
*   React
*   react-leaflet
*   Chakra UI
*   axios
*   leaflet

## Getting Started

Follow these steps to get the application up and running:

1. Clone the repository or download the source code files.

2. Open a terminal and navigate to the project directory.

3. Install the project dependencies by running the following command:

```
npm install

```

4. Once the dependencies are installed,start the development server:

```
npm run dev

```

5. The application will be accessible at your localhost in your browser.


## Usage

1. Enter a location search term in the input field.

2. Click the "Search" button to fetch matching locations.

3. The related matches will be displayed below the search input field.
    
4. Select a location from the displayed locations.

5. The map will update to show the selected location's marker, and a popup will display information about the location's population and population date (if available).

## File Structure

* 'src/App.jsx': The main component that renders the application.

* 'src/main.jsx': The entry point of the application.

* 'src/Home.jsx': This is the parent container for all other components.

* 'src/components/SearchBar.jsx': Containes Input Text field to search for a location.

* 'src/components/LocationsContainer.jsx': Containes the related search locations.

* 'src/components/Map.jsx': Containes Map Container.

* 'src/api.js': It containes api call function.

## Dependencies

* 'react': A JavaScript library for building user interfaces.

* 'react-dom': A package that serves as the entry point to the DOM and server renderers for React.

* 'react-leaflet': A React wrapper for the Leaflet mapping library.

* '@chakra-ui/react': A simple, modular, and accessible component library for React applications.

* 'axios': A popular JavaScript library for making HTTP requests.
    
* 'leaflet': An open-source JavaScript library for interactive maps.


## APIs

* OpenStreetMap Nominatim API: Used for geocoding and retrieving location information.

## Approach To Solve

* First I am storing the search text and passing it to getLocations function to make an api call with that search text and I am storing that result in 'locations' state variable and it is used to display related locations below the search bar.

* While displaying the locations I added a condition that the location has population or population date in it.If anyone of it present then only show that location.

* Then when we choose the location from the displayed locations then we get that selected location data from the locations data by using find.Then I am updating the position state variable that will make marker to fly to that location.

* After if I click on marker then it will display a popup with population and population date in it.

* And also if required number of boundaries are there in the data then it will display a highleted boundary also.


## Refrences

* [Leaflet tutorial](https://youtu.be/jD6813wGdBA)

* [Refrence video for how to use map and flyto method](https://youtu.be/zVtIttXDBrk)

* [Blog on leaflet](https://blog.logrocket.com/react-leaflet-tutorial/)

* [Importance of leaflet and difference between leaflet and react-leaflet](https://medium.com/@sefasonmez/leafletjs-react-leaflet-36915edbefb4)



