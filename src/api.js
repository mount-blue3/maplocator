import axios from "axios";

export const getLocations = async (address) => {
  try {
    const response = await axios.get(
      `https://nominatim.openstreetmap.org/?addressdetails=1&q=${address}&format=json&limit=50&extratags=1&polygon_geojson=1`
    );
    return response.data;
  } catch (error) {
    console.log(error);
    return [];
  }
};
