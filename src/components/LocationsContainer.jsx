import { Box, Heading } from "@chakra-ui/react";
import { v4 as uuidv4 } from "uuid";

function LocationsContainer({ locations, onSelect, searchInputRef }) {
  const uuid = uuidv4;
  let count = 0;

  return (
    <>
      <Box>
        {locations &&
          locations.length > 0 &&
          locations.map((location) => {
            if (
              location.extratags.population ||
              location.extratags["population:date"]
            ) {
              count++;
              return (
                <Heading
                  key={uuid()}
                  fontSize="1rem"
                  mb="0.5rem"
                  cursor="pointer"
                  onClick={() => onSelect(location.display_name)}
                >
                  {location.display_name}
                </Heading>
              );
            }
            return null;
          })}
      </Box>
      {count === 0 && searchInputRef?.current?.value !== undefined && (
        <Box>
          <Heading fontSize="1.5rem">No matches found</Heading>
        </Box>
      )}
    </>
  );
}

export default LocationsContainer;
