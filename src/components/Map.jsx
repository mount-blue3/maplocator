import { MapContainer, TileLayer, Marker, Popup, GeoJSON } from "react-leaflet";
import { Box, Flex, Text } from "@chakra-ui/react";
import "leaflet/dist/leaflet.css";

function Map({ position, populationRelatedData, boundaries, mapRef }) {
  if (mapRef.current && position) {
    mapRef.current.flyTo(position, 13, {
      duration: 2,
    });
  }

  const geoJSONStyle = {
    fillColor: "blue",
    weight: 2,
    opacity: 1,
    color: "blue",
    fillOpacity: 0.3,
  };

  let geoJSONData = null;
  if (boundaries) {
    geoJSONData = {
      type: "Feature",
      properties: {},
      geometry: {
        type: "Polygon",
        coordinates: boundaries,
      },
    };
  }

  return (
    <>
      <MapContainer
        ref={mapRef}
        center={position}
        zoom={13}
        style={{
          height: "600px",
          width: "90%",
          marginLeft: "4rem",
        }}
      >
        <TileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />
        {geoJSONData && boundaries[0] && boundaries[0].length > 3 && (
          <GeoJSON data={geoJSONData} style={geoJSONStyle} />
        )}
        <Marker position={position}>
          <Popup>
            <Box>
              <Flex alignItems="center" columnGap={2}>
                <Text fontWeight="bold" fontSize="1rem">
                  Population:{" "}
                </Text>
                <Text fontSize="1rem">
                  {populationRelatedData && populationRelatedData.population
                    ? populationRelatedData.population
                    : "Population data not available"}
                </Text>
              </Flex>
            </Box>
            <Box>
              <Flex alignItems="center" columnGap={2}>
                <Text fontWeight="bold" fontSize="1rem">
                  Population-Date:{" "}
                </Text>
                <Text fontSize="1rem">
                  {populationRelatedData && populationRelatedData.populationDate
                    ? populationRelatedData.populationDate
                    : "PopulationDate is not available"}
                </Text>
              </Flex>
            </Box>
          </Popup>
        </Marker>
      </MapContainer>
    </>
  );
}

export default Map;
