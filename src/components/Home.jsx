import React, { useState, useRef, useEffect } from "react";
import { Box, Spinner, Heading, List, ListItem } from "@chakra-ui/react";
import SearchBar from "./SearchBar";
import LocationsContainer from "./LocationsContainer";
import Map from "./Map";
import { getLocations } from "../api";

function Home() {
  const [locations, setLocations] = useState([]);
  const [populationRelatedData, setPopulationRelatedData] = useState({});
  const [boundaries, setBoundaries] = useState([]);
  const [position, setPosition] = useState({ lat: 42.361145, lon: -71.057083 });
  const [isLoading, setIsLoading] = useState(false);
  const [recentSearches, setRecentSearches] = useState(() => {
    const savedSearches = localStorage.getItem("recentSearches");
    return savedSearches ? JSON.parse(savedSearches) : [];
  });
  const searchInputRef = useRef();
  const mapRef = useRef(null);

  useEffect(() => {
    localStorage.setItem("recentSearches", JSON.stringify(recentSearches));
  }, [recentSearches]);

  const fetchData = async (searchText) => {
    setIsLoading(true);
    try {
      const response = await getLocations(searchText);
      setLocations(response);
    } catch (error) {
      console.log(error);
    } finally {
      setIsLoading(false);
    }
  };

  const handleSearch = (searchText) => {
    if (searchText) {
      searchInputRef.current.value = searchText;
    }
    fetchData(searchText);
    if (!recentSearches.includes(searchInputRef.current.value) && searchText)
      setRecentSearches([searchInputRef.current.value, ...recentSearches]);
  };

  const handleSelect = (value) => {
    const selectedLocation = locations.find(
      (location) => location.display_name === value
    );
    if (selectedLocation) {
      const { lat, lon } = selectedLocation;
      setPopulationRelatedData({
        population: selectedLocation.extratags.population,
        populationDate: selectedLocation.extratags["population:date"],
      });
      setBoundaries(selectedLocation.geojson.coordinates);
      setPosition({ lat: parseFloat(lat), lon: parseFloat(lon) });
    }
  };
  return (
    <Box>
      <SearchBar
        searchInputRef={searchInputRef}
        onSearch={() => handleSearch(searchInputRef.current.value)}
      />
      <Box mb="2rem" ml="2rem">
        {isLoading ? (
          <Box>
            <Spinner size="xl" color="blue.500" />
          </Box>
        ) : (
          <LocationsContainer
            locations={locations}
            onSelect={handleSelect}
            searchInputRef={searchInputRef}
          />
        )}
      </Box>
      <Box ml="2rem" mb="4rem">
        <Heading fontSize="1.5rem" mb="1rem">
          Recent searches
        </Heading>
        <List ml="1rem">
          {recentSearches.map(
            (result, index) =>
              index < 10 && (
                <ListItem
                  key={result}
                  cursor="pointer"
                  onClick={() => handleSearch(result)}
                >
                  <Heading fontSize="1rem" mb="0.5rem">
                    {result}
                  </Heading>
                </ListItem>
              )
          )}
        </List>
      </Box>
      <Map
        position={position}
        populationRelatedData={populationRelatedData}
        boundaries={boundaries}
        mapRef={mapRef}
      />
    </Box>
  );
}

export default Home;
