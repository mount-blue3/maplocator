import React from "react";
import { Input, Button, Box, Flex } from "@chakra-ui/react";

function SearchBar({ searchInputRef, onSearch }) {
  const handleSearchButtonClick = () => {
    onSearch();
  };
  return (
    <Box m="2rem">
      <Flex>
        <Input placeholder="Search for a location" ref={searchInputRef} />
        <Button colorScheme="blue" ml={2} onClick={handleSearchButtonClick}>
          Search
        </Button>
      </Flex>
    </Box>
  );
}

export default SearchBar;
